import threading
import time
from queue import Queue

from settings import SERVER_HOST, SERVER_PORT, USERNAME, PASSWORD, MOUNT_POINT, VERBOSE, PORT1, PORT2
from utils.gps import GPS
from utils.ntrip_client import NTRIPClient
from utils.common import logger


class SimpleRTK2B(threading.Thread):

    def __init__(self):
        super().__init__()
        self._ntrip_client = NTRIPClient(host=SERVER_HOST, port=SERVER_PORT, username=USERNAME, password=PASSWORD,
                                         mount_point=MOUNT_POINT)
        self._b_stop = threading.Event()
        self._b_stop.clear()
        self._cor_data = None
        self._lock = threading.RLock()
        self._queue = [Queue(maxsize=100), Queue(maxsize=100)]
        self.gps1 = GPS(port=PORT1, index=0, func_cor_data=self.get_cor_data, verbose=VERBOSE)
        self.gps2 = GPS(port=PORT2, index=1, func_cor_data=self.get_cor_data, verbose=VERBOSE)

    def run(self) -> None:
        self.gps1.start()
        self.gps2.start()
        while not self._b_stop.is_set():
            cor_data = self._ntrip_client.get_correction_data()
            if not self._queue[0].full():
                self._queue[0].put(cor_data)
            if not self._queue[1].full():
                self._queue[1].put(cor_data)
            time.sleep(.05)

    def get_cor_data(self, index):
        buf = []
        while not self._queue[index].empty():
            buf.append(self._queue[index].get())
        return b''.join(buf)

    def stop(self):
        self._b_stop.set()
        self.gps1.stop()
        self.gps2.stop()
        self.gps1.join()
        self.gps2.join()

    def get_data(self):
        return [self.gps1.get_data(), self.gps2.get_data()]


if __name__ == '__main__':

    logger.info('========== Starting Main Script ==========')
    last_print_time = 0

    app = SimpleRTK2B()
    app.start()

    fixed_state = [None, None]
    _last_time = [time.time(), time.time()]
    while True:
        try:
            data = app.get_data()
            for i in range(2):
                if data[i] and fixed_state[i] != data[i].get('fixed_state'):
                    logger.info(f"{i * '  '}#{i}: changed status from {fixed_state[i]} to {data[i]['fixed_state']}")
                    elapsed = round(time.time() - _last_time[i], 2)
                    if fixed_state[i] == '4':
                        logger.info(f"\t\t\tFIXED for {elapsed} sec")
                        _last_time[i] = time.time()
                    elif data[i].get('fixed_state') == '4':
                        logger.info(f"\t\t\tRecovered in {elapsed} sec")
                        _last_time[i] = time.time()
                    fixed_state[i] = data[i]['fixed_state']
            # Print data every minute.
            if time.time() - last_print_time > 60:
                # print(data)
                last_print_time = time.time()
            time.sleep(.5)
        except KeyboardInterrupt:
            break
    app.stop()
    app.join()
