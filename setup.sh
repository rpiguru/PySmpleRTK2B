cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"


apt update

python3 -m pip install -r requirements.txt

cp utils/90-ublox-udev.rules /etc/udev/rules.d/
service udev restart

cp ${cur_dir}/utils/simple_rtk2b.conf /etc/supervisor/conf.d/
sed -i -- "s/DIRECTORY/${cur_dir//\//\\/}/g" /etc/supervisor/conf.d/simple_rtk2b.conf

service supervisor start
service supervisor reload
