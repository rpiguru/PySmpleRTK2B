import socket
import base64
import os
import sys
import time
from datetime import datetime
import serial

_par_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
if _par_dir not in sys.path:
    sys.path.append(_par_dir)


from settings import SERVER_HOST, PASSWORD, USERNAME, SERVER_PORT, MOUNT_POINT, GPS_BAUDRATE, RTCM3_PACKET_SIZE
from utils.common import logger


class NTRIPClient(object):

    def __init__(self, host, port, username, password, mount_point, timeout=1, buffer_size=RTCM3_PACKET_SIZE):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.mount_point = mount_point
        self.timeout = timeout
        self.buffer_size = buffer_size
        self._socket = None
        self._logged_in = False

    def login(self):
        logger.debug('^^^ NTRIP: Logging in...')
        try:
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._socket.settimeout(self.timeout)
            self._socket.connect((self.host, self.port))
            pwd = base64.b64encode("{}:{}".format(self.username, self.password).encode())
            header = \
                "GET /{} HTTP/1.1\r\n".format(self.mount_point) + \
                "Host \r\n".format(self.host) + \
                "User-Agent: NTRIP pyUblox/0.0\r\n" + \
                "Authorization: Basic {}\r\n\r\n".format(pwd.decode())
            self._socket.send(header.encode())
            resp = self._socket.recv(1024).decode()
            logger.debug(f"^^^ NTRIP: Received response from server - {resp}")
            self._logged_in = "200 OK" in resp
            return self._logged_in
        except Exception as e:
            logger.error(f"^^^ NTRIP: Failed to login - {e}")

    def get_correction_data(self, timeout=2):
        buf = bytearray()
        s_time = time.time()
        while True:
            if len(buf) > 0:
                break
            if time.time() - s_time > timeout:
                return
            if self._logged_in or self.login():
                try:
                    recv = self._socket.recv(self.buffer_size)
                except Exception as e:
                    logger.error(f"Failed to receive from NTRIP server - {e}")
                    self._logged_in = False
                    self._socket = None
                    return
                buf += recv
        return buf


if __name__ == '__main__':

    import sys

    client = NTRIPClient(host=SERVER_HOST, port=SERVER_PORT, username=USERNAME, password=PASSWORD,
                         mount_point=MOUNT_POINT)

    if len(sys.argv) > 1:
        _port = sys.argv[1]
    else:
        _port = '/dev/ttyACM0'

    _ser = None

    _last_read_time = 0
    status = None

    while True:
        if _ser is None:
            try:
                _ser = serial.Serial(port=_port, baudrate=GPS_BAUDRATE, timeout=1, write_timeout=1)
            except serial.SerialException:
                _ser = None

        cor_data = client.get_correction_data()
        if cor_data and _ser is not None:
            try:
                written = _ser.write(cor_data)
                # Read data and check RTK fixed status in every 5 sec.
                if time.time() - _last_read_time > 5:
                    _line_data = _ser.readline()
                    if not _line_data.startswith(b'$GNTXT') and b'GNGGA' in _line_data:
                        index = _line_data.index(b'$GNGGA')
                        nmea_str = _line_data[index:].decode()
                        fixed_state = nmea_str.split(',')[6]
                        if status != fixed_state:
                            print(f"{datetime.now()} :: Changed from {status} to {fixed_state}")
                            status = fixed_state
                    _last_read_time = time.time()
            except Exception as er:
                print(f"Exception Occurred - {er}")
                _ser.close()
                _ser = None
        else:
            print("No COR data available!")
        time.sleep(.01)
