import os
import logging.config
from datetime import datetime, timedelta

_cur_dir = os.path.dirname(os.path.realpath(__file__))

logging.config.fileConfig(os.path.join(_cur_dir, 'logging.ini'))
logger = logging.getLogger('RTK')


def itow2datetime(itow):
    """
    Convert iTOW(milliseconds from the start of week) to datetime
    :param itow:
    :return:
    """
    week_start = datetime.now() - timedelta(days=datetime.now().weekday() + 1)
    week_start = week_start.replace(hour=0, minute=0, second=0, microsecond=0)
    # Leap seconds between GPS and UTC is 19
    return week_start + timedelta(milliseconds=itow) - timedelta(seconds=19)
