import threading
import time
from datetime import datetime

import serial

from settings import GPS_BAUDRATE
from utils.common import logger, itow2datetime
from utils.ubx2nmea import UBXParser


class GPS(threading.Thread):

    def __init__(self, port='/dev/ttyACM0', index=0, baudrate=GPS_BAUDRATE, func_cor_data=None, verbose=False):
        super().__init__()
        self.port = port
        self.baudrate = baudrate
        self.func_cor_data = func_cor_data
        self.verbose = verbose
        self._ser = None
        self._b_stop = threading.Event()
        self._b_stop.clear()
        self._data = {}
        self.index = index
        self._ubx_parser = UBXParser()
        self._cor_data = None
        self.nmea_data = None

    def _connect(self):
        try:
            self._ser = serial.Serial(port=self.port, baudrate=self.baudrate, timeout=.5, write_timeout=1)
            logger.info(f"Connected to the serial - {self.port}")
            return True
        except Exception as e:
            logger.error('Failed to open device({}) - {}'.format(self.port, e))
            self._ser = None

    def run(self) -> None:
        while not self._b_stop.is_set():
            try:
                if self._ser is not None or self._connect():
                    _line_data = self._ser.readline()
                    if not _line_data.startswith(b'$GNTXT'):
                        if b'GNGGA' in _line_data:
                            self.nmea_data = _line_data[_line_data.index(b'$GNGGA'):]
                        else:
                            self.nmea_data = None
                        self._parse_ubx_packet(_line_data)
                        if self.verbose:
                            print(f"{datetime.now()} :: {self.port} : {_line_data[:50]}")
                    time.sleep(.01)
                    cor_data = self.func_cor_data(self.index)
                    if cor_data != self._cor_data and cor_data is not None:
                        # self._ser.reset_output_buffer()
                        # self._ser.reset_input_buffer()
                        written = self._ser.write(cor_data)
                        if self.verbose:
                            print(f"{datetime.now()} :: {self.port} : Wrote {written} bytes of correction data")
                        self._cor_data = cor_data
                time.sleep(.05)
            except Exception as e:
                logger.error(f'{self.port}: Failed to read data from GPS - {e}')
                self._close_serial()

    def _parse_ubx_packet(self, packet):
        # if self.verbose:
        #     print(f"{datetime.now()} :: {self.port}: Parsing line - {packet}")
        if b'GNGGA' in packet:
            index = packet.index(b'$GNGGA')
            nmea_str = packet[index:].decode()
            fixed_state = nmea_str.split(',')[6]
            ubx_bytes = packet[:index]
        else:
            fixed_state = self._data.get('fixed_state')
            ubx_bytes = packet
        ubx_data = self._ubx_parser.parse(ubx_bytes)
        if ubx_data and type(ubx_data) == list:
            ubx = ubx_data[-1]
            self._data = {
                'LAT': ubx['LAT'] / 10000000. + ubx['LatHP'] / 1000000000.,
                'LON': ubx['LON'] / 10000000. + ubx['LonHP'] / 1000000000.,
                'ALT': ubx['HMSL'] + ubx['hMSLHp'] / 10.,
                'HEIGHT': ubx['HEIGHT'] + ubx['HeightHP'] / 10.,
                'hAcc': ubx['hAcc'] / 10.,
                'vAcc': ubx['vAcc'] / 10.,
                'fixed_state': fixed_state,
                'dt': itow2datetime(itow=ubx['ITOW']).isoformat(),
            }

    def _close_serial(self):
        if self._ser is not None:
            self._ser.close()
        self._ser = None

    def stop(self):
        self._b_stop.set()
        self._close_serial()

    def get_data(self):
        return self._data
