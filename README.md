# PySmpleRTK2B
SimpleRTK2B board adapter on Ubuntu

## Configuration of the SimpleRTK2B Board on u-center.

- Set `baudrate` as 115200
- Enable UBX mode and allow `UBX (NAV-HPPOSLLH)` **only**.
- Enable NMEA mode and allow `GNGGA` packet **only**.


## Installation

```shell script
sudo bash setup.sh
```

## Launch script manually

```shell script
python3 main.py
```
